#ifndef BOTUTILS_H
#define BOTUTILS_H

#include "types.h"
#include <QJsonObject>

class BotUtils
{
public:
    static ChatType toChatType(const QString& type);
    static bool buildCommand(const Message& message, Command& command);
    static void parseMessageJson(const QJsonObject& json, Message& msg);
};

#endif // BOTUTILS_H
