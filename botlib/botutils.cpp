#include "botutils.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QVector>

ChatType BotUtils::toChatType(const QString& type) {
    if(type == "private") {
        return ChatType::PRIVATE;
    }

    if(type == "group") {
        return ChatType::GROUP;
    }

    if(type == "supergroup") {
        return ChatType::SUPERGROUP;
    }

    if(type == "channel") {
        return ChatType::CHANNEL;
    }

    return ChatType::UNKNOWN;
}

bool BotUtils::buildCommand(const Message& message, Command& command) {
    if(message.text.startsWith("/")) {
        command.chat = message.chat;
        command.user = message.user;
        command.timestamp = message.timestamp;

        command.params = message.text.split(" ").toVector().toStdVector();

        command.name = command.params.begin()->remove("/");
        command.params.erase(command.params.begin());

        return true;
    }

    return false;
}

void BotUtils::parseMessageJson(const QJsonObject& json, Message& msg) {
    msg.text = json.value("text").toString();
    msg.timestamp = json.value("date").toDouble();

    QJsonObject chatObj = json.value("chat").toObject();
    msg.chat.reset(new Chat());

    msg.chat->id = chatObj.value("id").toDouble();
    msg.chat->title = chatObj.value("title").toString();
    msg.chat->firstname = chatObj.value("first_name").toString();
    msg.chat->lastname= chatObj.value("last_name").toString();
    msg.chat->username = chatObj.value("username").toString();
    msg.chat->type = toChatType(chatObj.value("type").toString());

    QJsonObject fromObj = json.value("from").toObject();
    msg.user.reset(new User());

    msg.user->id = fromObj.value("id").toDouble();
    msg.user->firstname = fromObj.value("first_name").toString();
    msg.user->lastname = fromObj.value("last_name").toString();
    msg.user->username = fromObj.value("username").toString();
}
