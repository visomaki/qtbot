#include "imodule.h"

IModule::IModule(const QString& name) : enabled(true), name(name) {}

IModule::~IModule() {}

void IModule::setEnabled(bool enabled) {
    this->enabled = enabled;
}

bool IModule::isEnabled() {
    return enabled;
}

const QString& IModule::getName() {
    return name;
}

const QSet<long>& IModule::getBlockedChats() {
    return blockedChats;
}

void IModule::blockChat(long chatId) {
    blockedChats.insert(chatId);
}

void IModule::unblockChat(long chatId) {
    blockedChats.remove(chatId);
}

bool IModule::isChatBlocked(long chatId) {
    return blockedChats.find(chatId) != blockedChats.end();
}
