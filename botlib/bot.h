#ifndef BOT_H
#define BOT_H

#include <QNetworkReply>
#include <QObject>
#include <QTimer>
#include "imodule.h"
#include "types.h"

class BotListener
{
public:
    virtual void messageReceived(const Message& message) = 0;
};

class Bot : public QObject
{
    Q_OBJECT

public:
    Bot(QObject* parent);
    virtual ~Bot();

    void sendMessage(const QString& text, long chatId);
    void start();
    void setListener(BotListener* listener);
    void setPort(int port);
    void setApiKey(const QString& apiKey);

    void registerModule(std::shared_ptr<IModule> module);
    void unregisterModule(std::shared_ptr<IModule> module);
    void clearModules();

    void setUpdateInterval(int interval);
    int getUpdateInterval();

    const QMap<long, std::shared_ptr<Chat> >& getChats();
    const QList<std::shared_ptr<IModule> >& getModules();

private slots:
    void replyFinished(QNetworkReply *reply);
    void fetchUpdates();

private:
    void notifyModules(const Message& message);

    void processMessage(const Message& message);
    void processCommand(const Command& command);

private:
    QList<std::shared_ptr<IModule> > modules;
    QMap<long, std::shared_ptr<Chat> > chats;

    BotListener* listener;
    QNetworkAccessManager *updateManager;
    QNetworkAccessManager *replyManager;
    QTimer* timer;

    QString apiKey;
    int port;
    long offset;
    int interval;
};

#endif // BOT_H
