#-------------------------------------------------
#
# Project created by QtCreator 2016-10-19T21:54:57
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = BotLib
TEMPLATE = lib
CONFIG += staticlib

SOURCES += bot.cpp \
    botutils.cpp \
    imodule.cpp

HEADERS += bot.h \
    botutils.h \
    imodule.h \
    types.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
