#ifndef IMODULE_H
#define IMODULE_H

#include <QString>
#include <QSet>

class Bot;
class Message;
class Command;

class IModule
{
public:
    IModule(const QString& name);
    virtual ~IModule();

    virtual void handleMessage(Bot* bot, const Message& message) = 0;
    virtual void handleCommand(Bot* bot, const Command& command) = 0;

    void setEnabled(bool enabled);
    bool isEnabled();

    const QString& getName();
    const QSet<long>& getBlockedChats();

    void blockChat(long chatId);
    void unblockChat(long chatId);
    bool isChatBlocked(long chatId);

private:
    bool enabled;
    QString name;
    QSet<long> blockedChats;
};

#endif // IMODULE_H
