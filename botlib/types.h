#ifndef TYPES_H
#define TYPES_H

#include <QString>
#include <QStringList>
#include <memory>

enum ChatType
{
    PRIVATE,
    GROUP,
    SUPERGROUP,
    CHANNEL,
    UNKNOWN
};

class User
{
public:
    long id;
    QString firstname;
    QString lastname;
    QString username;
};

class Chat
{
public:
    long id;
    QString title;
    QString firstname;
    QString lastname;
    QString username;
    ChatType type;
};

class Message
{
public:
    std::shared_ptr<User> user;
    std::shared_ptr<Chat> chat;
    std::shared_ptr<Message> replyTo;
    uint timestamp;
    QString text;
};

class Command
{
public:
    std::shared_ptr<User> user;
    std::shared_ptr<Chat> chat;
    std::vector<QString> params;
    uint timestamp;
    QString name;
};


#endif // TYPES_H
