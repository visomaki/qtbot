#include "bot.h"

#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include "botutils.h"

Bot::Bot(QObject* parent) : QObject(parent), listener(nullptr), offset(0), interval(2000) {
    updateManager = new QNetworkAccessManager(this);
    replyManager = new QNetworkAccessManager(this);

    connect(updateManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    timer = new QTimer(this);
    timer->setSingleShot(false);
    connect(timer, SIGNAL(timeout()), this, SLOT(fetchUpdates()));
}

Bot::~Bot() {
}

void Bot::start() {
    timer->start(interval);
}

void Bot::setListener(BotListener* listener) {
    this->listener = listener;
}

void Bot::setPort(int port) {
    this->port = port;
}

void Bot::setApiKey(const QString& apiKey) {
    this->apiKey = apiKey;
}

void Bot::setUpdateInterval(int interval) {
    this->interval = interval;
    if(timer->isActive()) {
        timer->start(interval);
    }
}

int Bot::getUpdateInterval() {
    return interval;
}

const QMap<long, std::shared_ptr<Chat> >& Bot::getChats() {
    return chats;
}

const QList<std::shared_ptr<IModule> >& Bot::getModules() {
    return modules;
}

void Bot::sendMessage(const QString& text, long chatId) {
    QUrl url(QString("https://api.telegram.org/bot") + apiKey + "/sendMessage");
    QUrlQuery params;

    params.addQueryItem("chat_id", QString::number(chatId));
    params.addQueryItem("text", text);
    params.addQueryItem("parse_mode", "Markdown");

    url.setQuery(params.query());
    url.setPort(port);

    QNetworkRequest request(url);
    replyManager->get(request);
}

void Bot::replyFinished(QNetworkReply * reply) {
    QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
    QJsonObject  data = json.object();
    QJsonValue status = data.value("ok");

    if(!status.toBool())
        return;

    QJsonArray results = data.value("result").toArray();

    foreach(QJsonValue result, results) {
        QJsonObject resultObj = result.toObject();

        Message msg;
        BotUtils::parseMessageJson(resultObj.value("message").toObject(), msg);

        if(listener) {
            listener->messageReceived(msg);
        }

        notifyModules(msg);

        long updateId = resultObj.value("update_id").toDouble();
        if(updateId >= offset) {
            offset = updateId + 1;
        }

        if(!chats.contains(msg.chat->id)) {
            chats.insert(msg.chat->id, msg.chat);
        }
    }
}

void Bot::fetchUpdates() {
    QUrl url("https://api.telegram.org/bot" + apiKey + "/getUpdates");
    QUrlQuery params;

    params.addQueryItem("offset", QString::number(offset));

    url.setQuery(params.query());
    url.setPort(port);

    QNetworkRequest request(url);
    updateManager->get(request);
}

void Bot::registerModule(std::shared_ptr<IModule> module) {
    modules.append(module);
}

void Bot::unregisterModule(std::shared_ptr<IModule> module) {
    modules.removeAll(module);
}

void Bot::clearModules() {
    modules.clear();
}

void Bot::notifyModules(const Message& message) {
    if(message.text.startsWith("/")) {
        Command command;
        if(BotUtils::buildCommand(message, command)) {
            processCommand(command);
        }
    }
    else {
        processMessage(message);
    }
}

void Bot::processMessage(const Message& message) {
    foreach(std::shared_ptr<IModule> module, modules) {
        if(module->isEnabled() && !module->isChatBlocked(message.chat->id)) {
            module->handleMessage(this, message);
        }
    }
}

void Bot::processCommand(const Command& command) {
    foreach(std::shared_ptr<IModule> module, modules) {
        if(module->isEnabled() && !module->isChatBlocked(command.chat->id)) {
            module->handleCommand(this, command);
        }
    }
}
