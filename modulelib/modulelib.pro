#-------------------------------------------------
#
# Project created by QtCreator 2016-10-20T17:48:49
#
#-------------------------------------------------

QT       += network
QT       -= gui

TARGET = modulelib
TEMPLATE = lib
CONFIG += staticlib

SOURCES += hellomodule.cpp

HEADERS += hellomodule.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../botlib/release/ -lBotLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../botlib/debug/ -lBotLib
else:unix: LIBS += -L$$OUT_PWD/../botlib/ -lBotLib

INCLUDEPATH += $$PWD/../botlib
DEPENDPATH += $$PWD/../botlib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/release/libBotLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/debug/libBotLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/release/BotLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/debug/BotLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../botlib/libBotLib.a
