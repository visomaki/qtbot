#ifndef HELLOMODULE_H
#define HELLOMODULE_H

#include "imodule.h"
#include <map>
#include <memory>
#include <QString>

class Node
{
public:
    Node();

    bool startNode;
    bool endNode;

    QString text;
    int usageCount;

    std::vector<std::pair<Node*, int> > next;
    std::vector<std::pair<Node*, int> > prev;

    void addNextNode(Node* node, int usageCount = 1);
    void addPrevNode(Node* node, int usageCount = 1);
};

class Markov
{
public:
    Markov();
    ~Markov();

    bool generateReply(const QString& text, QString& result);
    void processText(const QString& text);

    void saveToFile();
    void loadFromFile();

    ulong nodeCount();
    ulong useCount(const QString& word);

private:
    bool buildMessage(const QString& word, QString& result);

private:
    std::map<QString, Node*> nodes;
};


class HelloModule : public IModule
{
public:
    HelloModule(const QString& name);
    virtual ~HelloModule();

    void handleMessage(Bot* bot, const Message& message) override;
    void handleCommand(Bot* bot, const Command& command) override;

private:
    Markov markov;
};

#endif // HELLOMODULE_H
