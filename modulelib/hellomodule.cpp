#include "hellomodule.h"
#include <bot.h>

#include <QFile>

Node::Node() : startNode(false), endNode(false), usageCount(0) {
}

void Node::addNextNode(Node* node, int usageCount) {
    for(unsigned int i = 0; i < next.size(); i++) {
        if(next[i].first->text == node->text) {
            next[i].second++;

            std::sort(next.begin(), next.end(), [](std::pair<Node*, int>& a, std::pair<Node*, int>& b) {
                return b.second < a.second;
            });

            return;
        }
    }

    next.push_back(std::make_pair(node, usageCount));
}

void Node::addPrevNode(Node* node, int usageCount) {
    for(unsigned int i = 0; i < prev.size(); i++) {
        if(prev[i].first->text == node->text) {
            prev[i].second++;

            std::sort(prev.begin(), prev.end(), [](std::pair<Node*, int>& a, std::pair<Node*, int>& b) {
                return b.second < a.second;
            });

            return;
        }
    }

    prev.push_back(std::make_pair(node, usageCount));
}

Markov::Markov() {
    loadFromFile();
}

Markov::~Markov() {
    saveToFile();

    for(auto iter = nodes.begin(); iter != nodes.end(); iter++) {
        delete iter->second;
    }
}

void Markov::processText(const QString& message) {
    QStringList parts = message.simplified().split(' ');

    //Create or update nodes
    for(int i = 0; i < parts.size(); i++) {
        Node* node;
        auto search = nodes.find(parts.at(i));

        if(search != nodes.end()) {
            node = nodes[parts.at(i)];
            node->usageCount++;

            if(i == 0) {
                node->startNode = true;
            }

            if(i == parts.size() - 1) {
                node->endNode = true;
            }
        }
        else {
            node = new Node();
            node->text = parts.at(i);
            node->usageCount = 1;
            node->startNode = (i == 0);
            node->endNode = (i == parts.size() - 1);

            nodes[parts.at(i)] = node;
        }
    }

    //Update links
    for(int i = 0; i < parts.size(); i++) {
        Node* current = nodes[parts.at(i)];

        if(i > 0) {
            Node* prev = nodes[parts.at(i-1)];
            current->addPrevNode(prev);
        }

        if(i < parts.size()-1) {
            Node* next = nodes[parts.at(i+1)];
            current->addNextNode(next);
        }
    }
}

bool Markov::generateReply(const QString& text, QString& result) {
    QString current = "";
    int usageCount = -1;

    QStringList parts = text.simplified().split(' ');

    for(int i = 0; i < parts.size(); i++) {
        auto search = nodes.find(parts.at(i));
        if(search != nodes.end()) {
            if(usageCount == -1 || usageCount > nodes[parts.at(i)]->usageCount) {
                current = nodes[parts.at(i)]->text;
                usageCount = nodes[parts.at(i)]->usageCount;
            }
        }
    }

    if(current != "") {
        return buildMessage(current, result);
    }

    return false;
}


bool Markov::buildMessage(const QString& word, QString& result) {
    auto search = nodes.find(word);
    if(search != nodes.end()) {
        result = word;

        Node* current = nodes[word];

        while(!current->prev.empty() && result.size() <= 1000) {
            if(current->startNode && qrand() % 100 > 50) {
                break;
            }

            int max = current->prev.size() / 2 + 1;
            int index = qrand() % max;

            result = current->prev.at(index).first->text + " " + result;
            current = current->prev.at(index).first;
        }

        current = nodes[word];

        while(!current->next.empty() && result.size() <= 1000) {
            if(current->endNode && qrand() % 100 > 50) {
                break;
            }

            int max = current->next.size() / 2 + 1;
            int index = qrand() % max;

            result += " " + current->next.at(index).first->text;
            current = current->next.at(index).first;
        }

        return true;
    }

    return false;
}

void Markov::saveToFile()
{
    QFile::remove("words.txt.old");
    QFile::remove("links.txt.old");

     //Create sum backups!
    QFile::rename("words.txt", "words.txt.old");
    QFile::rename("links.txt", "links.txt.old");

    QFile file("words.txt");
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        for(auto iter = nodes.begin(); iter != nodes.end(); iter++) {
            stream << iter->second->text + " " +
                      QString::number(iter->second->usageCount) + " " +
                      QString::number(iter->second->startNode) + " " +
                      QString::number(iter->second->endNode);

            stream << endl;
        }
    }

    file.close();

    QFile file2("links.txt");
    if ( file2.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file2 );
        for(auto iter = nodes.begin(); iter != nodes.end(); iter++) {
            stream << iter->second->text + " ";

            for(int i = 0; i < iter->second->prev.size(); i++) {
                stream << iter->second->prev[i].first->text << " " << iter->second->prev[i].second << " ";
            }

            stream << endl;
            stream << iter->second->text + " ";

            for(int i = 0; i < iter->second->next.size(); i++) {
                stream << iter->second->next[i].first->text << " " << iter->second->next[i].second << " ";
            }

            stream << endl;
        }
    }

    file2.close();
}

void Markov::loadFromFile() {
    QFile file("words.txt");
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );

        while(!stream.atEnd())
        {
            QString line = stream.readLine();
            QStringList parts = line.split(" ");

            if(parts.size() == 4)
            {
                Node* node = new Node();
                node->text = parts.at(0);
                node->usageCount = parts.at(1).toInt();
                node->startNode = parts.at(2).toInt();
                node->endNode = parts.at(3).toInt();

                nodes[node->text] = node;
            }
        }
    }

    file.close();

    QFile file2("links.txt");
    if ( file2.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file2 );

        while(!stream.atEnd())
        {
            QString line = stream.readLine();
            QStringList parts = line.split(" ");

            if(parts.size() > 2)
            {
                Node* node = nodes[parts.at(0)];

                for(int i = 1; i < parts.size(); i+=2) {
                    if(parts.at(i).trimmed() != "") {
                        node->addPrevNode(nodes[parts.at(i)], parts.at(i+1).toInt());
                    }
                }
            }

            line = stream.readLine();
            parts = line.split(" ");

            if(parts.size() > 2)
            {
                Node* node = nodes[parts.at(0)];

                for(int i = 1; i < parts.size(); i+=2) {
                    if(parts.at(i).trimmed() != "") {
                        node->addNextNode(nodes[parts.at(i)], parts.at(i+1).toInt());
                    }
                }
            }
        }
    }

    file2.close();
}

ulong Markov::nodeCount() {
    return nodes.size();
}

ulong Markov::useCount(const QString& word) {
    auto search = nodes.find(word);
    if(search != nodes.end()) {
        return nodes[word]->usageCount;
    }

    return 0;
}

HelloModule::HelloModule(const QString& name) : IModule(name) {
}

HelloModule::~HelloModule() {
}

void HelloModule::handleMessage(Bot* bot, const Message& message) {
    markov.processText(message.text);

    if(message.text.toLower().contains("proffa"))
    {
        QString result;
        if(markov.generateReply(message.text, result)) {

            result.replace("proffa", message.user->firstname);
            result.replace("Proffa", message.user->firstname);

            bot->sendMessage(result, message.chat->id);
        }
    }
}

void HelloModule::handleCommand(Bot* bot, const Command& command) {
    if(command.name == "word_count") {
        bot->sendMessage("Olen oppinut jo " +  QString::number(markov.nodeCount()) + " sanaa!", command.chat->id);
    }

    if(command.name == "use_count" && command.params.size() == 1) {
        int count = markov.useCount(command.params[0]);

        if(count > 0) {
            bot->sendMessage("Olen kuullut sanan '" + command.params[0] + "' " + QString::number(count) + (count != 1 ? " kertaa!" : " kerran!"), command.chat->id);
        }
        else {
            bot->sendMessage("En ole kuullut sanaa '" + command.params[0] + "' aiemmin, kiitos nyt muistan sen!", command.chat->id);
            markov.processText(command.params[0]);
        }
    }
}
