#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "bot.h"

#include "hellomodule.h"
#include "moduledialog.h"

#include <QMenu>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    bot = new Bot(this);
    bot->setListener(this);
    bot->setPort(443);
    bot->setApiKey("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

    std::shared_ptr<IModule> module(new HelloModule("HelloModule"));
    bot->registerModule(module);

    bot->start();

    ui->intervalSpinBox->setValue(bot->getUpdateInterval());

    QList<std::shared_ptr<IModule> > modules = bot->getModules();
    foreach(std::shared_ptr<IModule> module, modules) {
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->moduleTreeWidget);

        updateModuleItem(item, module);
        ui->moduleTreeWidget->addTopLevelItem(item);
    }
}

MainWindow::~MainWindow()
{
    delete bot;
    delete ui;
}

void MainWindow::messageReceived(const Message& message)
{
    QTreeWidgetItem* item = new QTreeWidgetItem(ui->messageTreeView);

    item->setText(0, QDateTime::fromTime_t(message.timestamp).toString(Qt::DefaultLocaleShortDate));
    item->setText(1, message.user->firstname);
    item->setText(2, message.text);

    if(message.chat->type == ChatType::PRIVATE)
        item->setText(3, message.chat->firstname);
    else
        item->setText(3, message.chat->title);

    ui->messageTreeView->addTopLevelItem(item);
}

void MainWindow::on_intervalSpinBox_valueChanged(int val)
{
    bot->setUpdateInterval(val);
}

void MainWindow::on_moduleTreeWidget_customContextMenuRequested(const QPoint &pos) {
    QTreeWidgetItem* item = ui->moduleTreeWidget->itemAt(pos);

    if(item) {
        int index = ui->moduleTreeWidget->indexOfTopLevelItem(item);
        std::shared_ptr<IModule> module = bot->getModules().at(index);

        QMenu menu;

        QAction* enableAction = menu.addAction("Enabled");
        QAction* propertiesAction = menu.addAction("Properties");

        enableAction->setCheckable(true);
        enableAction->setChecked(module->isEnabled());

        QAction* action = menu.exec(ui->moduleTreeWidget->mapToGlobal(pos));

        if(action == enableAction) {
            module->setEnabled(!module->isEnabled());
            enableAction->setChecked(module->isEnabled());

            updateModuleItem(item, module);
        }
        else if(action == propertiesAction) {

            ModuleDialog dialog;
            dialog.init(module, bot);
            dialog.exec();
        }
    }
}

 void MainWindow::updateModuleItem(QTreeWidgetItem* item, std::shared_ptr<IModule> module)
 {
     item->setText(0, module->getName());
     item->setText(1, module->isEnabled() ? "Yes" : "No");
 }

void MainWindow::on_pushButton_clicked()
{
    auto chats = bot->getChats();
    foreach(auto chat, chats) {
        if(chat->type == ChatType::GROUP) {
            bot->sendMessage(ui->messageLineEdit->text(), chat->id);
        }
    }
}

