#ifndef UIUTIL_H
#define UIUTIL_H

#include <types.h>

class UIUtil
{
public:
    static QString getChatTypeString(ChatType type);
};

#endif // UIUTIL_H
