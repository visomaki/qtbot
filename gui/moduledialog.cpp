#include "moduledialog.h"
#include "ui_moduledialog.h"

#include <uiutil.h>

ModuleDialog::ModuleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ModuleDialog)
{
    ui->setupUi(this);
}

ModuleDialog::~ModuleDialog()
{
    delete ui;
}

void ModuleDialog::init(std::shared_ptr<IModule> module, Bot* bot) {
    this->module = module;
    this->bot = bot;

    setWindowTitle(module->getName());

    QMap<long, std::shared_ptr<Chat> > chats = bot->getChats();

    foreach(std::shared_ptr<Chat> chat, chats) {
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->chatTreeWidget);

        if(chat->type == ChatType::PRIVATE) {
            item->setText(0, chat->firstname);
        }
        else {
            item->setText(0, chat->title);
        }

        item->setText(1, UIUtil::getChatTypeString(chat->type));
        item->setText(2, module->isChatBlocked(chat->id) ? "Yes" : "");

        ui->chatTreeWidget->addTopLevelItem(item);
    }
}
