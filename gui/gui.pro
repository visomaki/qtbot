#-------------------------------------------------
#
# Project created by QtCreator 2016-10-19T23:09:21
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gui
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    moduledialog.cpp \
    uiutil.cpp

HEADERS  += mainwindow.h \
    moduledialog.h \
    uiutil.h

FORMS    += mainwindow.ui \
    moduledialog.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../botlib/release/ -lBotLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../botlib/debug/ -lBotLib
else:unix: LIBS += -L$$OUT_PWD/../botlib/ -lBotLib

INCLUDEPATH += $$PWD/../botlib
DEPENDPATH += $$PWD/../botlib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/release/libBotLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/debug/libBotLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/release/BotLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../botlib/debug/BotLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../botlib/libBotLib.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../modulelib/release/ -lmodulelib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../modulelib/debug/ -lmodulelib
else:unix: LIBS += -L$$OUT_PWD/../modulelib/ -lmodulelib

INCLUDEPATH += $$PWD/../modulelib
DEPENDPATH += $$PWD/../modulelib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../modulelib/release/libmodulelib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../modulelib/debug/libmodulelib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../modulelib/release/modulelib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../modulelib/debug/modulelib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../modulelib/libmodulelib.a
