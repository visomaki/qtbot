#include "uiutil.h"
#include "imodule.h"

QString UIUtil::getChatTypeString(ChatType type) {
    if(type == ChatType::CHANNEL) {
        return "channel";
    }

    if(type == ChatType::PRIVATE) {
        return "private";
    }

    if(type == ChatType::GROUP) {
        return "group";
    }

    if(type == ChatType::SUPERGROUP) {
        return "super group";
    }

    return "unknown";
}
