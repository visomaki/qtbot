#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>

#include "bot.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow, BotListener
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

   virtual void messageReceived(const Message& message);

private slots:
    void on_intervalSpinBox_valueChanged(int val);
    void on_moduleTreeWidget_customContextMenuRequested(const QPoint &pos);

    void on_pushButton_clicked();

private:
    void updateModuleItem(QTreeWidgetItem* item, std::shared_ptr<IModule> module);

private:
    Ui::MainWindow *ui;
    Bot* bot;
};

#endif // MAINWINDOW_H
