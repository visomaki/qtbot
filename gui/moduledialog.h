#ifndef MODULEDIALOG_H
#define MODULEDIALOG_H

#include <QDialog>
#include <bot.h>

namespace Ui {
class ModuleDialog;
}

class ModuleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ModuleDialog(QWidget *parent = 0);
    ~ModuleDialog();

    void init(std::shared_ptr<IModule> module, Bot* bot);

private:
    std::shared_ptr<IModule> module;
    Bot* bot;

    Ui::ModuleDialog *ui;
};

#endif // MODULEDIALOG_H
