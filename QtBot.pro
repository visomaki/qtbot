#-------------------------------------------------
#
# Project created by QtCreator 2016-10-17T18:16:06
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtBot

TEMPLATE  = subdirs
CONFIG   += ordered
SUBDIRS = botlib \
        modulelib \
        gui




